package com.staysanta.somevaluablerest;

public interface RemoteFileTestCases {
    enum FileCase {
        _1GB("http://speedtest.ftp.otenet.gr/files/test1Gb.db",
                "",
                "sha1Digest",
                "sha256Digest"),

        _100Mb("http://speedtest.ftp.otenet.gr/files/test100Mb.db",
                "",
                "sha1Digest",
                "20492a4d0d84f8beb1767f6616229f85d44c2827b64bdbfb260ee12fa1109e0e"),

        _10Mb("http://speedtest.ftp.otenet.gr/files/test10Mb.db",
                "",
                "sha1Digest",
                "e5b844cc57f57094ea4585e235f36c78c1cd222262bb89d53c94dcb4d6b3e55d"),

        _1Mb("http://speedtest.ftp.otenet.gr/files/test1Mb.db",
                "",
                "sha1Digest",
                "30e14955ebf1352266dc2ff8067e68104607e750abb9d3b36582b8af909fcb58");

        private final String source;
        private final String md5Digest;
        private final String sha1Digest;
        private final String sha256Digest;

        FileCase(String source, String digest, String sha1Digest, String sha256Digest) {
            this.source = source;
            this.md5Digest = digest;
            this.sha1Digest = sha1Digest;
            this.sha256Digest = sha256Digest;
        }

        public String getSource() {
            return source;
        }

        public String getMd5Digest() {
            return md5Digest;
        }

        public String getSha1Digest() {
            return sha1Digest;
        }

        public String getSha256Digest() {
            return sha256Digest;
        }
    }
}
