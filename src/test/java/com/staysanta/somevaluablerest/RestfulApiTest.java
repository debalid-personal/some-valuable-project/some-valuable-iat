package com.staysanta.somevaluablerest;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.palantir.docker.compose.DockerComposeRule;
import com.palantir.docker.compose.connection.DockerPort;
import com.palantir.docker.compose.connection.waiting.HealthChecks;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.ClassRule;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;

/**
 * Various utils to test API methods (including starting of docker-compose)
 */
public abstract class RestfulApiTest {
    protected interface Url {
        String TASKS = "tasks";
        String CANCEL = "tasks/cancel";
        String BATCH = "batch";
    }

    protected interface Algo {
        String MD5 = "md5";
        String SHA_1 = "sha-1";
        String SHA_256 = "sha-256";
    }

    @ClassRule
    public static DockerComposeRule docker = DockerComposeRule.builder()
            .file("src/test/resources/docker-compose.yml")
            .waitingForService("rest", HealthChecks.toRespond2xxOverHttp(4000, dockerPort -> dockerPort.inFormat("http://$HOST:$EXTERNAL_PORT/tasks")))
            .waitingForService("rabbit", HealthChecks.toHaveAllPortsOpen())
            .saveLogsTo("build/dockerLogs/dockerComposeRuleTest")
            .build();

    protected static final TypeReference<HashMap<String, Object>> MAP_TYPE_REFERENCE = new TypeReference<HashMap<String, Object>>() {
        //ignore
    };

    protected static final TypeReference<List<HashMap<String, Object>>> LIST_OF_MAP_TYPE_REFERENCE = new TypeReference<List<HashMap<String, Object>>>() {
        //ignore
    };

    protected static ObjectMapper createObjectMapper() {
        final ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new Jdk8Module());
        mapper.registerModule(new JavaTimeModule());
        return mapper;
    }

    protected ObjectMapper mapper = createObjectMapper();

    protected DockerPort restApi() {
        return docker.containers().container("rest").port(4000);
    }

    protected String restApiEndpoint(String url) {
        final DockerPort restApi = restApi();
        return String.format("http://%s:%s/%s", restApi.getIp(), restApi.getExternalPort(), url);
    }

    /**
     * doRequest is blocking, it's just a wrapper
     */
    protected void doRequest(HttpUriRequest request, BiConsumer<HttpResponse, CookieStore> consumer) {
        CookieStore store = new BasicCookieStore();
        CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultCookieStore(store).build();
        try (final CloseableHttpResponse response = httpClient.execute(request)) {
            consumer.accept(response, store);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    protected <T> T doRequest(HttpUriRequest request, BiFunction<HttpResponse, CookieStore, T> function) {
        CookieStore store = new BasicCookieStore();
        CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultCookieStore(store).build();
        try (final CloseableHttpResponse response = httpClient.execute(request)) {
            return function.apply(response, store);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    protected HttpEntity createTaskRunInput(String source, String algo) {
        return new StringEntity(String.format("{\"src\": \"%s\", \"algo\": \"%s\"}", source, algo), ContentType.APPLICATION_JSON);
    }

    protected HttpEntity createTaskCancelInput(UUID taskId) {
        return new StringEntity(String.format("{\"taskId\": \"%s\"}", taskId), ContentType.APPLICATION_JSON);
    }

    protected HttpEntity createBatchTaskRunInput(File source, String algo) {
        return MultipartEntityBuilder.create()
                .addTextBody("algo", algo)
                .addBinaryBody("file", source)
                .build();
    }

    protected Map<String, Object> readValueAsMap(HttpEntity entity) {
        try {
            return mapper.readValue(entity.getContent(), MAP_TYPE_REFERENCE);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    protected List<Map<String, Object>> readValueAsListOfMaps(HttpEntity entity) {
        try {
            return mapper.readValue(entity.getContent(), LIST_OF_MAP_TYPE_REFERENCE);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
