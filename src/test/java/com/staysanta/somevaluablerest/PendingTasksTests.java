package com.staysanta.somevaluablerest;

import com.google.common.collect.Sets;
import org.apache.http.Header;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.junit.Test;

import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.Matchers.equalToIgnoringCase;
import static org.junit.Assert.assertThat;

public class PendingTasksTests extends RestfulApiTest implements RemoteFileTestCases {
    /**
     * System should place a task in queue
     */
    @Test
    public void creationOfTaskShouldReturn201Success() throws IOException {
        final String endpoint = restApiEndpoint(Url.TASKS);
        HttpPut request = new HttpPut(endpoint);
        request.setEntity(createTaskRunInput(FileCase._10Mb.getSource(), Algo.MD5));
        doRequest(request, (response, store) -> {
            assertThat(response.getStatusLine().getStatusCode(), is(201));
        });
    }

    /**
     * System should have a persisted entity corresponded to placed task request
     */
    @Test
    public void creationOfTaskShouldCreatePendingOrStartedTask() throws IOException {
        final HttpPut httpPut = new HttpPut(restApiEndpoint(Url.TASKS));
        httpPut.setEntity(createTaskRunInput(FileCase._1GB.getSource(), Algo.MD5));

        String pendingTaskUrl = doRequest(httpPut, (response, cookie) -> {
            final Header location = response.getFirstHeader("Location");
            assertThat(location, is(notNullValue()));
            return location.getValue();
        });

        final Map<String, Object> json = doRequest(new HttpGet(pendingTaskUrl), ((response, cookie) -> {
            assertThat(response.getStatusLine().getStatusCode(), is(200));

            assertThat(response.getEntity().getContentType().getValue(), containsString(ContentType.APPLICATION_JSON.getMimeType()));

            return readValueAsMap(response.getEntity());
        }));

        assertThat(json.get("status").toString(), anyOf(equalToIgnoringCase("pending"), equalToIgnoringCase("started")));
    }

    /**
     * System should place all 10 tasks requests in queue
     */
    @Test
    public void creationOfSeveralTasksShouldQueueAllTheseTasks() throws IOException {
        final Set<String> requestedTaskIds = IntStream.rangeClosed(1, 10).boxed().map(x -> {
            final HttpPut httpPut = new HttpPut(restApiEndpoint(Url.TASKS));
            httpPut.setEntity(createTaskRunInput(FileCase._100Mb.getSource(), Algo.MD5));

            return doRequest(httpPut, (response, cookie) -> {
                assertThat(response.getStatusLine().getStatusCode(), is(201));

                return readValueAsMap(response.getEntity()).get("taskId").toString();
            });
        }).collect(Collectors.toSet());

        final Set<String> actualPlacedTaskIds = doRequest(new HttpGet(restApiEndpoint(Url.TASKS)), (response, cookies) -> {
            return readValueAsListOfMaps(response.getEntity()).stream()
                    .map(x -> x.get("id").toString())
                    .collect(Collectors.toSet());
        });

        assertThat(Sets.intersection(requestedTaskIds, actualPlacedTaskIds), is(requestedTaskIds));
    }
}
