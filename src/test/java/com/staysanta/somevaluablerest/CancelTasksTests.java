package com.staysanta.somevaluablerest;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.UUID;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CancelTasksTests extends RestfulApiTest implements RemoteFileTestCases {
    private static final Logger LOG = LoggerFactory.getLogger(CancelTasksTests.class);

    /**
     * Case: downloading a huge file ~ 1GB
     * If we make a cancel, system should cancel this in close scope of time (e.g. 1 minute)
     */
    @Test
    public void shouldCancelInMinute() throws InterruptedException {
        final HttpPut createTask = new HttpPut(restApiEndpoint(Url.TASKS));
        createTask.setEntity(createTaskRunInput(FileCase._1GB.getSource(), Algo.MD5));
        final String longRunningTaskId = doRequest(createTask, (response, cookies) -> {
            return readValueAsMap(response.getEntity()).get("taskId").toString();
        });

        final HttpPut cancelTask = new HttpPut(restApiEndpoint(Url.CANCEL));
        cancelTask.setEntity(createTaskCancelInput(UUID.fromString(longRunningTaskId)));
        doRequest(cancelTask, ((response, store) -> {}));

        int retries = 30;
        while (retries > 0) {
            final HttpGet getTaskState = new HttpGet(restApiEndpoint(Url.TASKS + "/" + longRunningTaskId));
            final String status = doRequest(getTaskState, ((response, store) -> {
                return readValueAsMap(response.getEntity()).get("status").toString();
            }));
            LOG.debug("Task id {} status {}", longRunningTaskId, status);
            if (status.equalsIgnoreCase("canceled")) {
                assertTrue(true);
                break;
            }
            Thread.sleep(Duration.ofSeconds(10).toMillis());
            retries--;
        }
        assertFalse(String.format("Task %s was not cancelled at time!", longRunningTaskId), true);
    }
}
