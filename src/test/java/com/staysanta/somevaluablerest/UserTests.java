package com.staysanta.somevaluablerest;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.cookie.Cookie;
import org.junit.Test;

import java.io.IOException;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class UserTests extends RestfulApiTest {
    private interface Const {
        String USER_ID = "userId";
    }

    @Test
    public void firstRequestShouldSetUserIdCookie() throws IOException {
        final String endpoint = restApiEndpoint(Url.TASKS);
        doRequest(new HttpGet(endpoint), (response, store) -> {
            Optional<Cookie> userIdOpt = store.getCookies().stream().filter(x -> Const.USER_ID.equals(x.getName())).findFirst();
            //assertThat(userIdOpt, is(optionalWithValue()))
        });
    }
}
