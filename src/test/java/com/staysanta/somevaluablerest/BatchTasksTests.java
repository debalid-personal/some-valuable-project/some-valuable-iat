package com.staysanta.somevaluablerest;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.collect.Streams;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.Matchers.equalToIgnoringCase;
import static org.junit.Assert.assertThat;

/**
 * TODO: These tests currently cover only those case when urls are unique
 */
public class BatchTasksTests extends RestfulApiTest {
    /**
     * All requested tasks should be processed by system with correct order
     */
    @Test
    public void shouldEnqueeAllTasksFromFile() throws URISyntaxException, IOException {
        Path testFile = Paths.get(BatchTasksTests.class.getClassLoader().getResource("digestBatchTest.txt").toURI());
        final List<String> requestedTasksEnqueued = Files.readAllLines(testFile);

        final HttpPut httpPut = new HttpPut(restApiEndpoint(Url.BATCH));
        httpPut.setEntity(createBatchTaskRunInput(testFile.toFile(), Algo.MD5));

        final Stream<String> actualTasksEnqueued = doRequest(httpPut, (response, cookies) -> {
            return readValueAsListOfMaps(response.getEntity()).stream()
                    .map(x -> x.get("src").toString());
        });

        //Tasks are expected to be enqueed with the same order as they were requested
        Streams.forEachPair(requestedTasksEnqueued.stream(), actualTasksEnqueued, (left, right) -> {
            assertThat(left, is(right));
        });
    }

    /**
     * All requested task should be actually created or started of finished
     */
    @Test
    public void shouldCreatePendingOrStartedTasksFromFile() throws URISyntaxException, IOException {
        Path testFile = Paths.get(BatchTasksTests.class.getClassLoader().getResource("digestBatchTest.txt").toURI());
        final List<String> requestedTasksToEnqueue = Files.readAllLines(testFile);

        final HttpPut httpPut = new HttpPut(restApiEndpoint(Url.BATCH));
        httpPut.setEntity(createBatchTaskRunInput(testFile.toFile(), Algo.MD5));

        doRequest(httpPut, (response, cookies) -> {
            //ignore
        });

        //Check that tasks actually were saved
        final List<Map<String, Object>> existingTasks = doRequest(new HttpGet(restApiEndpoint(Url.TASKS)), (response, cookies) -> {
            return readValueAsListOfMaps(response.getEntity());
        });

        final ImmutableMap<String, Map<String, Object>> taskBySrc = Maps.uniqueIndex(existingTasks, (x -> x != null ? x.getOrDefault("src", "-").toString() : null));

        requestedTasksToEnqueue.stream().forEach(x -> {
            final Map<String, Object> task = taskBySrc.get(x);
            assertThat(task, is(notNullValue()));
            assertThat(task.get("status").toString(), anyOf(
                    equalToIgnoringCase("pending"),
                    equalToIgnoringCase("started"),
                    equalToIgnoringCase("done")
            ));
        });
    }
}
